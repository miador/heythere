package com.yusuf.heythere;


public class Message {

    String msgText;
    String sender;
    String time;

    public Message() {
    }

    public Message(String msgText, String sender, String time) {
        this.msgText = msgText;
        this.sender = sender;
        this.time = time;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
